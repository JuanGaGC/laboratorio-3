#1-Escribir un programa en PYTHON que utilice una función lambda para sumar dos valores
#y multiplicarlos un tercer valor ingresado por el usuario, el programa debe realizar
#la operación hasta que el usuario indique lo contrario. 5 puntos
#Ejemplo:
#Ingrese numero 1: 10
#Ingrese numero 2: 20
#Ingrese numero 3: 15
#El resultado es:  450
#Desea volver a ingresar valores (s/n): n
#Gracias por utilizar el sistema
'''
opcion="s"
while opcion!="n":
    num1=int(input("Ingrese el número 1: "))
    num2=int(input("Ingrese el número 2: "))
    num3=int(input("Ingrese el número 3: "))
    a=num1+num2
    multi=lambda num3:num3*a
    print(a*num3)
    opcion=input("Desea continuar? \n"
                 "S=Si\n"
                 "N=No\n")

else:
    print("Gracias por utilizar el sistema")
'''

#2-Escribir en Python un programa que permita agregar valores a una lista
#y a un diccionario según sea la elección del usuario, el programa deberá
#continuar agregando elementos hasta que el usuario lo decida y cuando
#este finaliza de realizar la inserción de elementos, el programa deberá
#imprimir los elementos almacenados en ambas estructuras
#Los keys del diccionario deben ser generados aleatoriamente utilizando la función random
#Los índices de la lista deben empezar con el número 35 en adelante. 15 puntos
#Ejemplo:
#Pendiente
'''
lista=list()
diccinario={}
indice=35

import random as rd
opcion="s"
opcion=(input("Desea agregar elementos a las estructuras? (S/N)\n"
                        "S=Si\n"
                        "N=No\n"))
while opcion=="s":
    agregar=input("Lista o Diccionario? (L-D)\n"
                        "L=Lista\n"
                        "D=Diccionario\n")
    if agregar=="l":
        valor=int(input("Agregue un elemento: "))
        lista.append(valor)
        valor2=input("Desea agregar otro elemento? (S/N)\n"
                        "S=Si\n"
                        "N=No\n")
        if valor2=="s":
            continue
        else:
            break

    elif agregar=="d":
        key=rd.randint(1,100)
        valor=int(input("Ingrese un valor: "))
        diccinario={key:valor}
        valor2=input("Desea agregar otro valor? (S/N)\n"
                        "S=Si\n"
                        "N=No\n")
        if valor2=="s":
            continue
        else:
            break
    else:
        print("Opcion incorrecta, intente de nuevo")

print("Los elementos ingresados en la lista son:")
for indice,valor in enumerate(lista,start=35):
    print("El indice es: ",indice,"y el valor es: ",valor)

print("==============================================")

print("Los elemetos ingresados en el diccionario son:")
for k in (diccinario):
    print(k," : ",diccinario[k])
print("Gracias por utilizar el sistema..")
'''

'''
  Desea agregar elementos a las estructuras (S/N): S
	Lista o Diccionario (L-D): L
		Agregue el elemento: 65
		Desea Agregar otro elemento a la lista (s/n) s
	Lista o Diccionario (L-D): D
		Agregue el elemento: 80
		Desea Agregar otro elemento al diccionario (s/n) n
Desea agregar elementos a las estructuras (S/N) N
El detalle de los elementos ingresados es:
Lista:
	Índice: 35 valor :65
Diccionario:
	Key: 5 valor: 80
Gracias por utilizar el sistema
'''

#3-Programar una clase en python que reciba un texto cualquiera ingresado
#por el usuario y lo imprima de forma inversa. 5 puntos
#Entrada: "Hola"
#Salida: "aloH"
'''
class Texto:
    words=input("Digite un texto cualquiera: ")
    texto1=words[::-1]
    print(texto1)
'''


#4-Escribir una clase en python llamada CalculaFigura que pueda devolver por medio
# de dos métodos (calcular_area y calcular_perímetro), el área y el perímetro de
# tres figuras diferentes (triangulo equilatero, cuadrado,circulo) . 25 puntos
#1.	La selección de la figura es ingresada por el usuario, así como los valores
#correspondientes a la fórmula de cada figura
#2.	La clase debe tener un contructor con un atributo TEXTO que imprima por
#defecto un saludo al usuario
#3.	El programada debe calcular el área o el perímetro de las figuras hasta
#que el usuario yo no desee realizar más cálculos.
#Ejemplo:
#HOLA ESTE ES MI PROGRAMA CALCULADORA
#Ingrese la figura (T=Triangulo – O=Circulo – C =Cuadrado): T
#Que desea Calcular (Area o Perimetro): A
#Ingrese los valores para:
#		Base en cm: 10
#		Altura en cm: 20
#El área del triángulo es: 100 centímetros
#Desea realizar un nuevo Calculo (s/n): n
#Gracias por utilizar nuestra calculadora

print("Hola, este es mi programa")
print("==================================")

calcular=input("¿Qué desea calcular? (a/p)\n"
               "a=Area\n"
               "p=Perimetro\n")
print("==================================")
figura=input("Cuál figura desea calcular? \n"
             "t=Triangulo \n"
             "c=Cuadrado \n"
             "o=Circulo \n")

import math

class calculaFigura:
    def CalcularArea(self,pFigura,pCalcular):
        self.figura=pFigura
        self.calcula=pCalcular
        while calcular=="a":
            if figura=="t":
                b=int(input("Ingrese la base en cm: "))
                h=int(input("Ingrese la altura en cm: "))
                areaTriangulo=b*h/2
                print ("El area del triangulo es: ",areaTriangulo,"centimetros")
                calc2=input("Desea calcular el area de otra figura? (s/n)\n"
                        "s=Si\n"
                        "n=No\n")
                if calc2=="s":
                    return(calcular)
                else:
                    break
            if figura=="c":
                a=int(input("Ingrese el area en cm: "))
                areaCuadrado= pow(a,2)
                print("El area del cuadrado es: ",areaCuadrado,"centimetros")
                calc2=input("Desea calcular el area de otra figura? (s/n)\n"
                        "s=Si\n"
                        "n=No\n")
                if calc2=="s":
                    return(calcular)
                else:
                    break
            if figura=="o":
                pi=3.14
                radio=int(input("Ingrese el valor del radio en cm: "))
                areaCirculo=3.14*pow(r,2)
                print("El area de un circulo es: ",areaCirculo,"centimetros")
                calc2=input("Desea calcular el area de otra figura? (s/n)\n"
                        "s=Si\n"
                        "n=No\n")
                if calc2=="s":
                    return(calcular)
                else:
                    break

    def CalcularPerimetro(self,pFigura,pCalcular):
        self.figura=pFigura
        self.calcula=pCalcular
        while calcular=="p":
            if figura=="t":
                a=int(input("Ingrese el valor de a: "))
                b=int(input("Ingrese el valor de b: "))
                c=int(input("Ingrese el valor de c: "))
                perimetro=a+b+c
                print("El perimetro de un triangulo es: ",perimetro)
                calc2=input("Desea calcular el perimetro de otra figura? (s/n)\n"
                        "s=Si\n"
                        "n=No\n")
                if calc2=="s":
                    return(calcular)
                else:
                    break
            if figura=="c":
                a=int(input("Ingrese el valor de a: "))
                perimetro=4*a
                print("El perimetro de un cuadrado es: ",perimetro)
                calc2=input("Desea calcular el perimetro de otra figura? (s/n)\n"
                        "s=Si\n"
                        "n=No\n")
                if calc2=="s":
                    return(calcular)
                else:
                    break
            if figura=="o":
                pi=3.14
                radio=int(input("Ingrese el valor del radio: "))
                longitud=2*pi*r
                print("El perimetro de un circulo es: ",longitud)
                calc2=input("Desea calcular el perimetro de otra figura? (s/n)\n"
                        "s=Si\n"
                        "n=No\n")
                if calc2=="s":
                    return(calcular)
                else:
                    break
        else:
            opcion=input("Desea realizar un nuevo calculo? (s/n): \n"
                  "s=Si\n"
                  "n=No\n")
            while opcion=="s":
                return(calcular)

            else:
                print("Gracias por utilizar nuestra calculadora")

